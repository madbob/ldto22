# Linux Day Torino 2022

https://linuxdaytorino.org/2022/

## Compilazione

```
npm install
```

Modificare solo i files che si trovano nella cartella source/ e poi eseguire

```
npm run build
```

## Tema

Copyright (c) 2016 - Present, Designed & Developed by [Themefisher](https://themefisher.com)

Released under the [MIT](https://github.com/themefisher/constra/blob/main/LICENSE) license.
